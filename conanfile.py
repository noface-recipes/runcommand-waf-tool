from conans import ConanFile

class Recipe(ConanFile):
    name        = "runcommand-waf-tool"
    version     = "0.1.0"
    license     = "unlicense"
    description = "Simple waf tool to run executables built on the project"
    url         = "https://gitlab.com/no-face/runcommand-waf-tool.git"

    build_policy = "missing"
    exports      = "*.py"

    def package(self):
        self.copy("run_command.py", "bin")
